def display(puzzle):
    print("   ----------------------------------")
    for x in range(len(puzzle)):
        print("   | ", end="")
        for y in range(len(puzzle[x])):
            if puzzle[x][y] == 0:
                print('.', end="  ")
            else:
                print(puzzle[x][y], end="  ")
            if (y + 1) % 3 == 0:
                print("| ", end="")
        print()
        if (x + 1) % 3 == 0:
            print("   ----------------------------------")


def is_valid_row(puzzle, row):
    check_values = [ False ] * 10

    for number in puzzle[row]:
        if number and check_values[number]:
            return False
        check_values[number] = True
    
    return True

def is_valid_column(puzzle, column):
    check_values = [ False ] * 10

    for row in range(9):
        number = puzzle[row][column]
        if number and check_values[number]:
            return False
        check_values[number] = True

    return True


def is_valid_block(puzzle, position):
    check_values = [ False ] * 10

    row, col = position

    row_start = (row // 3) * 3
    col_start = (col // 3) * 3

    for row in range(row_start, row_start + 3):
        for col in range(col_start, col_start + 3):
            number = puzzle[row][col]
            if number and check_values[number]:
                return False
            check_values[number] = True

    return True


def is_valid_puzzle(puzzle):
    for i in range(9):
        if is_valid_row(puzzle, i) == False:
            return False
        
        if is_valid_column(puzzle, i) == False:
            return False

    block_elements = [ (0, 0), (0, 3), (0,6), (3, 0), (3,3), (3, 6), (6, 0), (6,3), (6, 6) ]
    
    for pos in block_elements:
        if is_valid_block(puzzle, pos) == False:
            return False

    return True


def possible_values_in_row(puzzle, row):
    row_values = set(range(1, 10))

    for x in puzzle[row]:
        row_values.discard(x)

    return row_values

def possible_values_in_column(puzzle, column):
    column_values = set(range(1, 10))

    for row in range(9):
        column_values.discard(puzzle[row][column])
    
    return column_values

def possible_values_in_block(puzzle, element_position):
    row, column = element_position

    # we will get the block position if we divide row // 3 and column // 3
    # get the starting row and column of the block where element presents
    start_row = (row // 3) * 3
    start_column = (column // 3) * 3

    block_values = set(range(1, 10))
    for row in range(start_row, start_row + 3):
        for column in range(start_column, start_column + 3):
            block_values.discard(puzzle[row][column])
    
    return block_values

def possible_values_in_cell(puzzle, position):
    row, column = position
    if puzzle[row][column] != 0:
        return { puzzle[row][column] }

    a = possible_values_in_row(puzzle, row)
    b = possible_values_in_block(puzzle, position)
    c = possible_values_in_column(puzzle, column)
    return a.intersection(b,c)

def is_valid_value(puzzle, position, new_number):
    row_index, col_index = position

    # check row
    for number in puzzle[row_index]:
        if number == new_number:
            return False

    # check column
    for i in range(len(puzzle)):
        number = puzzle[i][col_index]
        if number == new_number:
            return False

    # check block

    # we will get the block position if we divide row // 3 and column // 3
    # get the starting row and column of the block where element presents
    row_start = (row_index // 3) * 3
    col_start = (col_index // 3) * 3

    for row in range(row_start, row_start + 3):
        for col in range(col_start, col_start + 3):
            number = puzzle[row][col]
            if number == new_number:
                return False

    return True


def empty_cells(puzzle):
    cells_to_fill = []
    for x in range(len(puzzle)):
        for y in range(len(puzzle)):
            if puzzle[x][y] == 0:
                cells_to_fill.append((x,y))
    return cells_to_fill


def empty_cell(puzzle):
    for x in range(len(puzzle)):
        for y in range(len(puzzle)):
            if puzzle[x][y] == 0:
                return (x, y)
    return


from sudoku_solver import is_valid_row, is_valid_column, is_valid_block, is_valid_puzzle, is_valid_value, empty_cells, empty_cell, possible_values_in_cell

def sudoku_puzzle():
    puzzle = [ 
        [ 6, 0, 0, 0, 0, 5, 0, 0, 0 ],
        [ 0, 9, 0, 4, 0, 0, 2, 0, 0 ],
        [ 0, 2, 8, 9, 6, 0, 0, 0, 4 ],
        [ 0, 0, 0, 1, 0, 3, 4, 0, 8 ],
        [ 0, 0, 0, 0, 0, 8, 0, 9, 3 ],
        [ 0, 8, 9, 7, 0, 4, 6, 0, 0 ],
        [ 8, 1, 0, 0, 0, 0, 9, 0, 6 ],
        [ 4, 0, 0, 0, 0, 0, 8, 3, 2 ],
        [ 9, 0, 0, 0, 4, 0, 1, 0, 0 ]
    ]

    return puzzle

def test_is_valid_row():
    puzzle = sudoku_puzzle()
    puzzle[0][8] = 6
    assert is_valid_row(puzzle, 0) == False

    puzzle[3][0] = 1
    assert is_valid_row(puzzle, 3) == False

    puzzle[0][8] = 0
    assert is_valid_row(puzzle, 0) == True

    puzzle[7][2] = 1
    assert is_valid_row(puzzle, 7) == True

def test_is_valid_column():
    puzzle = sudoku_puzzle()
    puzzle[0][4] = 6
    assert is_valid_column(puzzle, 4) == False

    puzzle[0][8] = 8
    assert is_valid_column(puzzle, 8) == False

    puzzle[0][4] = 2
    assert is_valid_column(puzzle, 4) == True

    puzzle[3][7] = 8
    assert is_valid_column(puzzle, 7) == True

def test_is_valid_block():
    puzzle = sudoku_puzzle()
    puzzle[0][1] = 6
    assert is_valid_block(puzzle, (0,1)) == False

    puzzle[4][4] = 3
    assert is_valid_block(puzzle, (4, 4)) == False

    puzzle[2][5] = 3
    assert is_valid_block(puzzle, (2, 5)) == True

    puzzle[8][8] = 4
    assert is_valid_block(puzzle, (8, 8)) == True

def test_is_valid_puzzle():
    puzzle = sudoku_puzzle()
    assert is_valid_puzzle(puzzle) == True

    puzzle[4][4] = 1
    assert is_valid_puzzle(puzzle) == False

    puzzle[0][7] = 9
    assert is_valid_puzzle(puzzle) == False

    puzzle[3][0] = 1
    assert is_valid_puzzle(puzzle) == False

    puzzle[4][4] = 2
    puzzle[0][7] = 1
    puzzle[3][0] = 5
    assert is_valid_puzzle(puzzle) == True

def test_is_valid_value():
    puzzle = sudoku_puzzle()
    assert is_valid_value(puzzle, (2, 0), 6) == False

    def test_row():
        assert is_valid_value(puzzle, (0, 7), 6) == False
        assert is_valid_value(puzzle, (1, 8), 9) == False
        assert is_valid_value(puzzle, (0, 8), 9) == True
        assert is_valid_value(puzzle, (2, 0), 1) == True

        # check about the modification of the puzzle
        assert sudoku_puzzle() == puzzle


    def test_column():
        assert is_valid_value(puzzle, (5, 0), 6) == False
        assert is_valid_value(puzzle, (5, 1), 2) == False
        assert is_valid_value(puzzle, (2, 6), 3) == True
        assert is_valid_value(puzzle, (0, 6), 3) == True
        assert sudoku_puzzle() == puzzle

    def test_block():
        assert is_valid_value(puzzle, (1, 2), 6) == False
        assert is_valid_value(puzzle, (8, 7), 6) == False
        assert is_valid_value(puzzle, (2, 0), 1) == True
        assert is_valid_value(puzzle, (1, 8), 7) == True
        assert sudoku_puzzle() == puzzle

    test_row()
    test_column()
    test_block()

def test_is_valid_puzzle():
    puzzle = sudoku_puzzle()
    assert is_valid_puzzle(puzzle) == True

    puzzle[0][1] = 6
    assert is_valid_puzzle(puzzle) == False

    puzzle[0][1] = 0
    puzzle[0][7] = 4
    assert is_valid_puzzle(puzzle) == False

    puzzle[0][6] = 1
    assert is_valid_puzzle(puzzle) == False

def test_empty_cells():
    puzzle = sudoku_puzzle()
    cells_to_fill = empty_cells(puzzle)
    
    empty_cells_count = 0
    for row in range(9):
        for col in range(9):
            if puzzle[row][col] == 0:
                assert cells_to_fill[empty_cells_count] == (row, col)
                empty_cells_count += 1


def test_possible_values_in_cell():
    puzzle = sudoku_puzzle()
    assert possible_values_in_cell(puzzle, (5, 0)) == { 1, 2, 3, 5 }
    assert possible_values_in_cell(puzzle, (1, 7)) == { 1, 5, 6, 7, 8 }
    assert possible_values_in_cell(puzzle, (0, 0)) == { 6 }

def test_empty_cell():
    puzzle = sudoku_puzzle()
    assert empty_cell(puzzle) == (0, 1)

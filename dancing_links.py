class Node:
    def __init__(self, data, column):
        self.data = data
        self.left = self
        self.right = self
        self.top = self
        self.bottom = self
        self.column = column


class ColumnHead:
    def __init__(self, name):
        self.name = name
        self.size = 0
        self.left = self
        self.right = self
        self.top = self
        self.bottom = self

class DoublyList:
    def __init__(self, name):
        self.__head = ColumnHead(name)

    def add_down(self, data):
        new_node = Node(data, self.__head)
        self.__head.size += 1

        last_node = self.__head.top
        last_node.bottom = new_node
        new_node.top = last_node
        new_node.bottom = self.__head
        self.__head.top = new_node

    def get_head(self):
        return self.__head

class DancingLinks:
    def __init__(self):
        self.root = None

    def create_column(self, name, data):
        # create a doubly linked list
        new_column = DoublyList(name)

        for value in data:
            new_column.add_down(value)

        return new_column.get_head()

    def add_column(self, name, data):
        new_column = self.create_column(name, data)
        if self.root:
            # get the last column in the dancing links
            last_column = self.root.left
            new_node = new_column

            # link every node to the last column node of the dancing links
            for _ in range(len(data)):
                new_node.right = last_column.right
                last_column.right = new_node
                new_node.left = last_column
                new_node.right.left = new_node
                last_column = last_column.bottom
                new_node = new_node.bottom
            return
        
        self.root = new_column

    def create(self, matrix):
        header = matrix[0]   
        rows = len(matrix)
        columns = len(header)
        
        for i in range(1, rows):
            column = [ ]
            for j in range(columns):
                column.append(matrix[i][j])
            self.add_column(header[i], column)



    def cover():
        pass

    def uncover():
        pass

    def remove_column():
        pass





def main():
    links = DancingLinks()
    even_numbers = [ 2, 4, 6, 8, 10, 12, 14 ]
    odd_numbers = [ 1, 3, 5, 7, 9, 11, 13 ]
    prime_numbers =[ 17, 19, 23, 29, 31, 37, 41 ]
    links.add_column("Even", even_numbers)
    links.add_column("Odd", odd_numbers)
    links.add_column("Prime", prime_numbers)
    links.test()

if __name__ == "__main__":
    main()


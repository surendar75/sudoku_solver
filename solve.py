import copy

from sudoku_solver import *
from timer import timer

''' In this file there are different type of solutions implemted
        solve by brain uses gather '''

def solve_by_brain(puzzle):
    cells_to_fill = empty_cells(puzzle)
    for empty_cell_pos in cells_to_fill:
        possible_values = possible_values_in_cell(puzzle, empty_cell_pos)
        if len(possible_values) == 1:
            value = possible_values.pop(0)
            row, col = empty_cell_pos
            puzzle[row][col] = value

    if len(cells_to_fill) == 0:
        return
    else:
        solve(puzzle)

def btrack_solve(puzzle):
    current_cell = empty_cell(puzzle)
    if not current_cell:
        return puzzle

    for value in range(1, 10):
        if is_valid_value(puzzle, current_cell, value):
            x, y = current_cell
            puzzle[x][y] = value
            solution = btrack_solve(puzzle)
            if solution:
                return solution
            puzzle[x][y] = 0

    return None

def main():
    puzzle = [ 
               [ 0, 0, 2, 0, 0, 5, 0, 8, 7 ],
               [ 0, 8, 5, 0, 0, 0, 4, 9, 0 ],
               [ 0, 7, 4, 9, 2, 0, 0, 0, 0 ],
               [ 0, 0, 0, 0, 9, 0, 0, 0, 0 ],
               [ 4, 0, 6, 1, 0, 7, 0, 0, 0 ],
               [ 0, 5, 3, 0, 8, 4, 0, 0, 0 ],
               [ 0, 0, 0, 0, 7, 2, 9, 4, 8 ],
               [ 0, 4, 9, 8, 3, 0, 0, 7, 5 ],
               [ 0, 2, 7, 0, 4, 0, 6, 3, 1 ],
            ]
    
    display(puzzle)
    print(3 * "\n")
    puzzle = btrack_solve(puzzle)
    display(puzzle)

if __name__ == "__main__":
    main()

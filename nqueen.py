# This python file contains the example of dancing links with 4 X 4 queen problem

''' Check the constrains. If the constrain fails return False otherwise True '''

from copy import deepcopy

def check_row(board, index):
    queen_count = 0
    for cell in board[index]:
        if cell:
            queen_count += 1
    
    return False if queen_count > 1 else True



def check_column(board, index):
    queen_count = 0
    for r in range(len(board)):
        if board[r][index]:
            queen_count += 1

    return False if queen_count > 1 else True




def check_diagonal(board, pos):
    queen_count = 0
    board_length = len(board)
    row, column = pos

    # move forward from the given cell
    for i, j in zip(range(row + 1, board_length), range(column + 1, board_length)):
        if board[i][j]:
            queen_count += 1

    # move back from the given cell
    for i, j in zip(range(row - 1, -1, -1), range(column - 1, -1, -1)):
        if board[i][j]:
            queen_count += 1

    return False if queen_count > 1 else True


def is_valid_state(board, pos):
    board_length = len(board)
    row, column = pos
    return check_row(board, row) and check_column(board, column) and check_diagonal(board, pos)


def propagation(board, pos):
    new_board = deepcopy(board)
    row, column = pos
    board_length = len(board)

    # row
    for j in range(board_length):
        if j != column:
            new_board[row][j] = 'X'

    # column
    for i in range(board_length):
        if i != row:
            new_board[i][column] = 'X'

    # diagonal
    # move forward
    i, j = row + 1, column + 1
    while i < board_length and j < board_length:
        new_board[i][j] = 'X'
        i += 1
        j += 1
    
    # move backward
    i, j = row  - 1, column - 1
    while i >= 0 and j >= 0:
        new_board[i][j] = 'X'
        i -= 1
        j -= 1

    return new_board


def exact_cover(board):
    return






''' The Brute Force Solution '''

def main():
    board = [ [ 0 for _ in range(8) ] for _ in range(8) ]
    print(propagation(board, (0,0)))


main()

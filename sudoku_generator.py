from random import randrange, shuffle
from sudoku_solver import is_valid_value, display
from solve import solve2



def random_sudoku():
    sudoku_puzzle = [ [ 0 for _ in range(9) ] for x in range(9) ]

    cells_to_fill = 25
    cells_filled = 0

    while cells_filled != cells_to_fill:
        row = randrange(0, 9)
        column = randrange(0, 9)

        if sudoku_puzzle[row][column] == 0:
            numbers = [ x for x in range(1, 10) ]
            shuffle(numbers)

            for number in numbers:
                if is_valid_value(sudoku_puzzle, (row, column), number):
                    sudoku_puzzle[row][column] = number
                    cells_filled += 1
                    break

    
    return sudoku_puzzle


def main():
    sudoku = random_sudoku()
    display(sudoku)

    solutions = solve2(sudoku)
    for puzzle in solutions:
        display(puzzle)


if __name__ == "__main__":
    main()




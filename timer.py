import time

''' *args: is a positional arguments of a function
    **kwargs is a dictonary keyword arguments
'''

def timer(f):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = f(*args, **kwargs)
        stop_time = time.time()
        dt = stop_time - start_time
        print(f"time_taken: {dt}")
        return result
    
    return wrapper
